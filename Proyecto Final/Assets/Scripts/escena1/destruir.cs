using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destruir : MonoBehaviour
{
    public GameObject jugador;
    public GameObject plataformaPrefab;
    private GameObject nuevaplataforma;
    public GameObject plataformaboostPrefab;


    private int pointerNube = 5;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.name.StartsWith("plataforma"))
        {
        if(Random.Range(1,7) == 1)
        {
            Destroy(collision.gameObject);
            Instantiate(plataformaboostPrefab, new Vector2(Random.Range(-4.5f,4.5f), jugador.transform.position.y + (pointerNube + Random.Range(0.2f, 1.0f))), Quaternion.identity);
        }else
        {
            collision.gameObject.transform.position = new Vector2(Random.Range(-4.5f,4.5f), jugador.transform.position.y + (pointerNube + Random.Range(0.2f,1.0f)));
         }
    }else if(collision.gameObject.name.StartsWith("plataformaboost"))
    {
            if(Random.Range(1, 20) == 1)
            {
                collision.gameObject.transform.position = new Vector2(Random.Range(-4.5f,4.5f), jugador.transform.position.y + (pointerNube + Random.Range(0.2f,1.0f)));
            }
            else
            {
            Destroy(collision.gameObject);
            Instantiate(plataformaPrefab, new Vector2(Random.Range(-4.5f, 4.5f), jugador.transform.position.y + (pointerNube + Random.Range(0.2f, 1.0f))), Quaternion.identity);
            }
        }
    }
}
