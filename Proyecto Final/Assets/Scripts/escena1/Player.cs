using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Rigidbody2D))]
public class Player : MonoBehaviour
{
    public float movementSpeed = 10f;
    private Rigidbody2D rb;
    private float movement;
    private float topPuntaje = 0.0f;
    public Text puntaje;
    public Animator anim;

    private int vidas;
    private string prefsvidas = "vidas";

    public GameObject vida1, vida2, vida3;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        anim.SetBool("muerto",false);
        loadData();
        cargarContadorVidas();
    }

    void FixedUpdate(){
        movement = Input.GetAxis("Horizontal");
        rb.velocity = new Vector2(movement * movementSpeed, rb.velocity.y);
    }

    void Update(){

        if(movement < 0){
            this.GetComponent<SpriteRenderer>().flipX = true;
        }else{
            this.GetComponent<SpriteRenderer>().flipX = false;
        }

        if(rb.velocity.y > 0 && transform.position.y > topPuntaje){
            topPuntaje = transform.position.y;
        }
        puntaje.text = "Score:" + Mathf.Round(topPuntaje).ToString();

        //si cae al vacio
        if(transform.position.y+30 < topPuntaje)
        {
            if(vidas <= 1){
                vidas = 3;
                saveData();
                SceneManager.LoadScene("Menu");
            }else{
                vidas--;
                saveData();
                SceneManager.LoadScene("GameScene");
            }
            //SceneManager.LoadScene("Menu");
            //SceneManager.LoadScene("GameScene");
        }

        //condicion para ganar
        if(Mathf.Round(topPuntaje) == 200f){
                vidas = 3;
                saveData();
                SceneManager.LoadScene("Menu");
            }
            //SceneManager.LoadScene("Menu");
            //SceneManager.LoadScene("GameScene");
        }

    public void Explotar() {
        StartCoroutine("CoroutineExplotar");
    }

    IEnumerator CoroutineExplotar(){
            anim.SetBool("muerto",true);    
            movementSpeed = 0f;
            yield return new WaitForSeconds(1.3f);
            if(vidas <= 1){
                vidas = 3;
                saveData();
                SceneManager.LoadScene("Menu");
            }else{
                vidas--;
                saveData();
                SceneManager.LoadScene("GameScene");
            }
    }

    private void saveData(){
        PlayerPrefs.SetInt(prefsvidas,vidas);
    }

    private void loadData(){
        vidas = PlayerPrefs.GetInt(prefsvidas,3);
    }

    private void cargarContadorVidas(){
        switch(vidas){
            case 1:
            vida1.GetComponent<SpriteRenderer>().enabled = true;
            vida2.GetComponent<SpriteRenderer>().enabled = false;
            vida3.GetComponent<SpriteRenderer>().enabled = false;
            break;

            case 2:
            vida1.GetComponent<SpriteRenderer>().enabled = true;
            vida2.GetComponent<SpriteRenderer>().enabled = true;
            vida3.GetComponent<SpriteRenderer>().enabled = false;
            break;

            case 3:
            vida1.GetComponent<SpriteRenderer>().enabled = true;
            vida2.GetComponent<SpriteRenderer>().enabled = true;
            vida3.GetComponent<SpriteRenderer>().enabled = true;
            break;

            default:
            vida1.GetComponent<SpriteRenderer>().enabled = true;
            vida2.GetComponent<SpriteRenderer>().enabled = true;
            vida3.GetComponent<SpriteRenderer>().enabled = true;
            break;
        }
    }
}
