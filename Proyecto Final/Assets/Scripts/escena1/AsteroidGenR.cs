using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidGenR : MonoBehaviour
{
    [SerializeField] private GameObject asteroide;
    [SerializeField] private float maxRandom;

    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine("generarAsteroide");
    }

    // Update is called once per frame
    void Update()
    {
        spawnAsteroides();
    }
    
    IEnumerator generarAsteroide(){
            Instantiate(asteroide, transform.position,Quaternion.identity);
            yield return new WaitForSeconds(3f);
    }

    private void spawnAsteroides(){
        float num = Random.Range(0f,maxRandom);
        Debug.Log(num);
        if(num > 4.5 && num < 4.6){
            Instantiate(asteroide, transform.position,Quaternion.identity);
        }
    }
}
