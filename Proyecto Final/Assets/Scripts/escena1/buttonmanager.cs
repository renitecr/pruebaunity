using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class buttonmanager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void startGame(){
        SceneManager.LoadScene("GameScene");
    }

    public void endGame(){
        Application.Quit();
    }

    public void startHardGame(){
        SceneManager.LoadScene("GameScene2");
    }
}
