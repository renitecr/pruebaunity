using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlExplosion : MonoBehaviour
{
    public GameObject astronauta;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.tag == "asteroide"){
            astronauta.GetComponent<Player>().Explotar();
        }
    }
}
