using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroideHard : MonoBehaviour
{
    [SerializeField] private float velocidad;
    private float rotationSpeed = 30;
    Vector3 currentEulerAngles;

    // Start is called before the first frame update
    void Start()
    {
        
    }
    // Update is called once per frame
    void Update()
    {
        //transform.Translate(Vector2.left * velocidad * Time.deltaTime);
        transform.position = transform.position + new Vector3(-1 * 2 * Time.deltaTime, 0 * 0 * Time.deltaTime, 0);
        currentEulerAngles += new Vector3(0, 0, 1) * Time.deltaTime * rotationSpeed;
        transform.eulerAngles = currentEulerAngles;
    }

}
