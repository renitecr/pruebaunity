# PruebaUnity

Proyecto del intersemestral de desarrollo de videojuegos Unity.

El juego consiste en saltar plataformas sin chocar con los asteroides, las plataformas impulsan automaticamente al jugador y este solo se mueve.

El 90% de los sprites fueron hechos a mano con la herramienta piskel, solo el mensaje de congratulations fue utilizado de otras fuentes.

Integrantes:

-> Sebastian Chaidez de la Rocha

-> Giovanni Alberto Salas Atondo



Actividades:

-> Sebastian Chaidez: 

     Creacion de los sprites de los asteroides y de la animacion de muerte.

     Sistema de vidas con guardado en la memoria.

     Ajuste de la dificultad del juego(modo normal y dificil).

     Flujo de las escenas y la logica para cada transicion de las mismas.

     Desarrollo de las colisiones del personaje con los asteroides y el sistema de generacion de asteroides.

     Desarrollo de la pantalla de juego ganado.

-> Giovanni Salas:

     Creacion del nucleo del juego (sistema de generacion de plataformas).

     Creacion de los sprites del personaje principal y movimiento del mismo.

     Creacion del menu de inicio.

     Creacion de los sprites del fondo y las plataformas.

     Creacion del sistema de puntuacion.
